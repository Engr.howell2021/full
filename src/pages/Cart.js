import React from 'react'
import {Link, Navigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import ProductCard from '../components/ProductCard'

import UserContext from '../UserContext'


export default function Cart() {

	const {user} = useContext(UserContext)
	
	const [cart, setCart] = useState([])

	useEffect(() => {

		fetch(`http://localhost:4000/orders/myOrders`,{
			method: 'GET',
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setCart(data.map(product => {

				return (

					<ProductCard key={product._id} cartProp={product}/>

				)
			}))

		})

	}, [])

	return(
		
		(user.isAdmin === true) ?

		<Navigate to ="/"/>
		
		:
	<>
		<h1 className="py-3 title">My Cart</h1>
		{cart}
	</>
	)
}
