import React from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import{useParams, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

import '../App.css'


export default function SpecificProduct(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [ProductName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const {productId} = useParams();
	const [quantity, setQuantity] = useState(1);

	const add = () =>{
		setQuantity(quantity + 1)
	}

	const minus = () =>{
		setQuantity(quantity - 1)

		if (quantity === 1) {
			setQuantity(1)
		}
	}

const addToCart = (productId) => {

		fetch('http://localhost:4000/orders/createorder', {
			method: 'POST',
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity,
				
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log("show data")
			console.log(data)

			if (data === true){
				Swal.fire({
					title: `Added to cart`,
					icon: "success",
					text: `You have successfully added ${productId} in your cart`,
					
				})
				history("/")
			} else {
				Swal.fire({
					title: "error",
					icon: "error",
					text: "Go Back",
					
				})
			}
		})
	}


	useEffect(() => {

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductName(data.ProductName);
			setDescription(data.description);
			setPrice(data.price)

		})

	}, [productId])

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card >
						<Card.Body className="containerDesign">
							<Card.Title className = "pt-3 title">{ProductName}</Card.Title>

							<Card.Subtitle className = "pb-2 pt-3">Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle className = "py-2">Price</Card.Subtitle>
							<Card.Text className="title">Php {price}</Card.Text>
														
							
							{ user.accessToken !== null ?
							<>
							<Card.Subtitle className = "py-2">Quantity: {quantity}</Card.Subtitle>
							<Card.Text >
							<Button variant="dark" onClick={minus}>-</Button>
							<Button className="mx-2" variant="dark" onClick={add}>+</Button>
							</Card.Text>
							<Button variant="success" onClick={()=>addToCart(productId)}>Checkout</Button>
							<Link className="btn btn-danger mx-2" to="/products">Go Back</Link>
							</>
							:
							<>
							<Link className="btn btn-dark mt-3 px-4 " to="/login">Login</Link>
							<Card.Text className="text-muted mt-2">Log in to use cart!</Card.Text>
							<Card.Text as={Link} to="/products" className="text-muted">Go back</Card.Text>
							</>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}
