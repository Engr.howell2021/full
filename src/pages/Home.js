import React from "react";

import Banner from '../components/Banner'
// import Highlights from '../components/Highlights'
import Footer from "../components/Footer";
// import UserContext from '../UserContext';
// import { useContext } from 'react';
import HomeBanner from "../components/HomeBanner";


const data = {
   

    title: 'NEW SHOES COLLECTIONS',
    content: 'Sale up to 80% off'
}

export default  function Home(){
   

    return (
        
    
        <>
        <HomeBanner bannerData={data}/>
        <Banner/>
        {/* <Highlights/> */}
        <Footer/>
        </>
       
       
        
    );
};
