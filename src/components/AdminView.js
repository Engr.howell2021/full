import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import AddProducts from './AddProducts';
import EditProducts from './EditProducts';
import ArchivedProducts from './ArchivedProducts';



export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([])

	//=============Getting the productsData from the products page
	useEffect(() => {
		const productsArr = productsData.map(product => {
			return (
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.ProductName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Availabe" : "Unavailabe"}
					</td>	
					<td><EditProducts product={product._id} fetchData={fetchData}/></td>
					<td><ArchivedProducts product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
				</tr>


				)
		})

		setProducts(productsArr)

	}, [productsData])



	//=============end of useEffect for productsData


	return(
		<>
			<div className="text-center my-4">
				<h1> Admin Dashboard</h1>
				<AddProducts fetchData={fetchData}/>

				
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan={2}>Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
			
		</>

		)
}