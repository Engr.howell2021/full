import React from 'react'
import { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Cart from './pages/Cart';
// import Order from './pages/Order';
import SpecificProduct from './pages/SpecificProduct';
import PageNotFound from './components/PageNotFound';
import { Container} from 'react-bootstrap';

import { UserProvider } from './UserContext';


// For routes
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom'


function App() {

  // React contex
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin')  === 'true'

  });
  // function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  };
 
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar/>
    <Container>
      <Routes>   
        < Route path="/" element={ <Home/>}/>
        < Route path="/products" element={ <Products/>}/>
        < Route path="/login" element={ <Login/>}/>
        < Route path="/register" element={ <Register/>}/>
        < Route path="/logout" element={ <Logout/>}/>
        < Route path="/cart" element={ <Cart/>}/>
        {/* < Route path="/order" element={ <Order/>}/> */}
        < Route path="/products/:productId" element={ <SpecificProduct/>}/>
        < Route path="*" element= {<PageNotFound />}/> 
      </Routes>
    </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
